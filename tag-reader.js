var ffprobe = require('./ffprobe-edited');

function normalizeTags(tags) {
  var normal = { title:null, artist:null, album:null, band:null, track:null, date:null };
  if (tags) {
    switch (tags.format.format_name) {
      case 'ogg':
        if (tags.streams.length > 0)
          normal = normalizeOgg(normal, tags.streams[0]);
        break;
      default:
        normal = normalizeMp3(normal, tags.metadata);
    }
    // Cleanup
    if (normal.track)
      normal.track = parseInt(normal.track);
    if (normal.artist === normal.band)
      normal.band = null;
  }
  return normal;
}

function normalizeMp3(normal, metadata) {
  // Add all the tags that are already normalized
  for (var i in normal) {
    if (metadata[i])
      normal[i] = metadata[i];
  }
  // Add some stragglers
  var regexBand = /album.+artist/i;
  var regexYear = /date|year/i;
  for (var i in metadata) {
    if (regexBand.test(i)) {
      // album artist
      normal.band = metadata[i];
    } else if (!normal.date && regexYear.test(i)) {
      // year, if date wasn't already found
      normal.date = metadata[i];
    }
  }
  return normal;
}

function normalizeOgg(normal, stream) {
  normal.title = stream['TAG:TITLE'];
  normal.artist = stream['TAG:ARTIST'];
  normal.album = stream['TAG:ALBUM'];
  normal.band = stream['TAG:ALBUM ARTIST'];
  normal.track = stream['TAG:track'];
  normal.date = stream['TAG:DATE'];
  return normal;
}

function reader(path, cb) {
  ffprobe(path, function (err, tags) {
    if (err) {
      cb(err, null);
    } else {
      var data = tags;
      data.normalized = normalizeTags(tags);
      data.normalized.file = tags.file;
      data.normalized.format = tags.format.format_name;
      data.normalized.probe_time = tags.probe_time;
      cb(null, data);
    }
  });
};

reader.sync = function readerSync(path) {
  var tags = ffprobe.sync(path);
  if (tags.error) {
    return tags;
  } else {
    var data = tags;
    data.normalized = normalizeTags(tags);
    data.normalized.file = tags.file;
    data.normalized.format = tags.format.format_name;
    data.normalized.probe_time = tags.probe_time;
    return data;
  }
};

module.exports = reader;