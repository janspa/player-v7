var express = require('express');
var dbFunctions = require('../db-functions');
var sanitize = require('../sanitize');

var router = express.Router({
    caseSensitive: true,
    strict       : true
});

module.exports = router;

function requireAuth(req, res, next) {
  var authed = true;
  if (authed)
    next();
  else
    res.status(500).json({ error: 'Failed to authenticate' });
}

//router.all('*', requireAuth);

router.get('/song/', function (req, res) {
  var sanitized = sanitize(req.query);
  req.mongoose.models.Song.find(sanitized, 'title artist band album track date format', function (err, docs) {
    if (err)
      res.send(err);
    else
      res.send(docs);
  });
});

router.get('/library/', function (req, res) {
  req.mongoose.models.Song.aggregate()
  .project({ artist: { $ifNull: [ "$band", "$artist" ] }, _id:0 })
  .group({ _id: "$artist" })
  .sort({ _id:1 })
  .exec(function (err, docs) {
    if (err)
      res.send(err);
    else {
      var reduced = [];
      docs.forEach(function(d) { reduced.push(d._id); });
      res.send(reduced);
    }
  });
});

router.get('/library/:artist', function (req, res) {
  req.mongoose.models.Song.distinct('album', {
    $or: [ { band: req.params.artist },
      { $and: [ { band: null }, { artist: req.params.artist } ] }
    ]
   })
  .exec(function (err, docs) {
    if (err)
      res.send(err);
    else
      res.send(docs);
  });
});

router.get('/library/:artist/:album', function (req, res) {
  req.mongoose.models.Song.find({
    album: req.params.album,
    $or: [ { band: req.params.artist },
      { $and: [ { band: null }, { artist: req.params.artist } ] }
    ]
  })
  .sort('track')
  .select('title artist band album track date format')
  .exec(function (err, docs) {
    if (err)
      res.send(err);
    else
      res.send(docs);
  });
});

router.get('/cover/:artist/:album', function (req, res) {
  req.mongoose.models.Cover.findOne({
    album: req.params.album,
    artist: req.params.artist
   })
  .select('file')
  .exec(function (err, doc) {
    if (err || !doc)
      res.status(404).end();
    else
      res.sendFile(doc.file, function (err) {
        if (err) res.status(err.status).end();
      });
  });
});

//router.param('id', /^[a-z0-9]+$/);

router.get('/songdata/:id', function(req, res) {
  req.mongoose.models.Song.findOne({_id:req.params.id}, 'file', function (err, doc) {
    if (err || !doc)
      res.status(404).end();
    else
      res.sendFile(doc.file, function (err) {
        if (err) res.status(err.status).end();
      });
  });
});