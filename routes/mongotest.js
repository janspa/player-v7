var express = require('express');
var dbFunctions = require('../db-functions');

var router = express.Router({
    caseSensitive: true,
    strict       : true
});

router.get('/', function (req, res) {
  req.mongoose.models.Song.find({}).exec(function (err, songs) {
    if (err) console.error(err);
    res.render('index', {
      title: 'Ayy',
      items: songs
    });
  });
});

router.post('/add/', function (req, res) {
  var songData = {};
  ['title', 'artist', 'album', 'band', 'track'].forEach(function(i) {
    songData[i] = req.body[i];
  });
  var models = req.mongoose.models;
  dbFunctions.addSong(models, songData, function(err, product) {
    res.redirect('..');
  });
});



router.get('/test', function (req, res) {
  res.send('1');
});
router.get('/test/', function (req, res) {
  res.send('2');
});

module.exports = router;