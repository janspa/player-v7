module.exports = function(mongoose) {
  var Schema = mongoose.Schema;

  var SongSchema = Schema({
    title: String,
    artist: String,
    band: String,
    album: String,
    track: Number,
    date: String,
    file: String,
    format: String
  });
  SongSchema.virtual('albumArtist').get(function() {
    return this.band || this.artist;
  });

  var CoverSchema = Schema({
    artist: String,
    album: String,
    file: String
  });

  return {
    Song:  mongoose.model('Song', SongSchema),
    Cover: mongoose.model('Cover', CoverSchema)
  };
};
