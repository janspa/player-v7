var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');

var mongoose = require('./db');
var tagReader = require('./tag-reader');

var app = express();

// Config

app.enable('strict routing');
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname,'views'));


// Middleware

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(bodyParser.urlencoded({ extended:false }));


mongoose.connection.once('open', function() {
  console.log("Mongoose connected.");
});
app.use(function(req, res, next) {
    req.mongoose = mongoose;
    next();
});


// Routes

app.all('/test', function (req, res) { res.redirect(301, '/test/'); });
app.use('/test/', require('./routes/mongotest'));
app.all('/api', function (req, res) { res.redirect(301, '/api/'); });
app.use('/api/', require('./routes/api'));

app.get('/', function (req, res) {
  res.send('ayy lmao');
});

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

if (app.get('env') == 'development') {
  app.use(function (err, req, res, next) {
      console.error(req.originalUrl, err);
      res.status(err.status || 500);
      res.send(err.message);
  });
}

app.use(function (err, req, res, next) {
  res.status(err.status || 500).send('Error!');
});


// Run server
app.listen(3000, function () {
  console.log("ready on port 3000");
});