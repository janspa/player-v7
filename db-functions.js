function handleDBError(err) {
  console.error(err);
}

function findOrCreate (model, condition, data, cb) {
  model.findOne(condition, function (err, result) {
    if (err || result) {
      cb(err, result, false);
    } else {
      var obj = new model(data);
      cb(err, obj, true);
    }
  });
}

function addSong (mongoose, data, cb) {
  var songCreated = function(err, song, created) {
      if (err) return handleDBError(err);
      //qLog("song", song.title, created);
      if (created) {
        song.save(cb);
      } else {
        cb(err, song);
      }
  };
  findOrCreate(mongoose.models.Song, {
    title: data.title,
    artist: data.artist,
    album: data.album
  }, data, songCreated);
}

function addCover (mongoose, data, cb) {
  var coverCreated = function(err, cover, created) {
      if (err) return handleDBError(err);
      //qLog("song", song.title, created);
      if (created) {
        cover.save(cb);
      } else {
        cb(err, cover);
      }
  };
  findOrCreate(mongoose.models.Cover, {
    artist: data.band || data.artist,
    album: data.album
   }, data, coverCreated);
}

module.exports.addSong = addSong;
module.exports.addCover = addCover;