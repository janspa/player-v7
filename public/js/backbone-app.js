(function($) {
  var LibItem = Backbone.Model.extend({
    defaults: { path: '', name: '' }
  });
  var LibItems = Backbone.Collection.extend({
    url: Pyreal.AppState.getApiUrl,
    model: LibItem,
    parse: function(res) {
      var c = [];
      for (var i = 0; i < res.length; i++)
        c.push({name:res[i], path: Pyreal.AppState.makeLibraryLink(res[i])});
      return c;
    }
  });

  var LibTrack = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      _id: '', title: '', artist: '', band: '', album: '', track: 0, date: '', format: ''
    }
  });
  var LibTracks = Backbone.Collection.extend({
    url: Pyreal.AppState.getApiUrl,
    model: LibTrack
  });

  Pyreal.View.register({
    name: 'lib-list',
    postInitialize: function() {
      this.collection = new LibItems;
    }
  });

  Pyreal.View.register({
    name: 'lib-tracks',
    elId: '#lib-list-container',
    postInitialize: function() {
      this.collection = new LibTracks;
    }
  });

  Pyreal.View.register({
    name: 'lib-breadcrumb',
    getTemplateData: function() {
      var parts = Pyreal.AppState.getBreadcrumb();
      var curIndex = Pyreal.AppState.getPath().length;
      var tplData = { nav: [] };
      for (var i = 0; i <= parts.length; i++) {
        var data = { className:'', url:'', title:'' };
        data.url = _.map(parts.slice(0,i), encodeURIComponent).join('/');
        data.title = (i) ? parts[i-1] : '«index»';
        if (i===curIndex)
          data.className = 'active';
        else if (i>curIndex)
          data.className = 'previous';
        tplData.nav.push(data);
      }
      return tplData;
    }
  });

  Pyreal.View.register({
    name: 'songinfo-cover',
    elId: '#songinfo-cover',
    postInitialize: function() {
      this.$el.find('img').on('error', function() {
        $(this).attr('src', '/img/pyreal.jpg');
      });
    },
    render: function() {
      this.$el.find('img').attr('src', Pyreal.AppState.getCoverUrl());
    }
  });

  $(document).ready(function() {

    var listView, tracksView, breadcrumbView,
        coverView;

    var Router = Backbone.Router.extend({
      initialize: function() {
        listView = Pyreal.View.create('lib-list');
        tracksView = Pyreal.View.create('lib-tracks');
        breadcrumbView = Pyreal.View.create('lib-breadcrumb');
        coverView = Pyreal.View.create('songinfo-cover');
      },

      routes: {
        "": "library",
        ":artist": "library",
        ":artist/:album": "tracks"
      },

      library: function (artist) {
        Pyreal.AppState.updatePath(artist);

        //tracksView.remove();
        listView.collection.fetch({
          reset: true,
          success: function() { listView.render(); breadcrumbView.render(); },
          error: function(col, res) { console.error(res); }
        });
      },

      tracks: function (artist, album) {
        Pyreal.AppState.updatePath(artist, album);
        //listView.remove();
        tracksView.collection.fetch({
          reset: true,
          success: function() { tracksView.render(); breadcrumbView.render(); },
          error: function(col, res) { console.error(res); }
        });
        coverView.render();
      }
    });

    new Router;
    Backbone.history.start({pushState: false});

  });


})(jQuery);