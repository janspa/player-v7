(function() {
  Pyreal = {};

  var libraryPath = [],
      breadcrumb  = [];


  Pyreal.AppState = {
    updatePath: function () {
      libraryPath = [].slice.call(arguments).filter(function(a) { return a!=null; });
      for (var i = 0; i < libraryPath.length; i++) {
        if (libraryPath[i] !== breadcrumb[i]) {
          breadcrumb = libraryPath.slice(0);
          break;
        }
      }
    },

    getApiUrl: function() {
      return '/api/library/'+_.map(libraryPath,encodeURIComponent).join('/');
    },

    getCoverUrl: function(array) {
      array = array || libraryPath;
      return '/api/cover/'+_.map(array,encodeURIComponent).join('/');
    },

    makeLibraryLink: function(item) {
      return _.union(libraryPath,[item]).map(encodeURIComponent).join('/');
    },

    getPath: function() {
      return libraryPath;
    },
    getBreadcrumb: function() {
      return breadcrumb;
    }
  };

}).call(this);