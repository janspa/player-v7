(function() {
  _ = this._ != null ? this._ : require('underscore');
  Backbone = this.Backbone != null ? this.Backbone : require('backbone');
  Handlebars = this.Handlebars != null ? this.Handlebars : require('handlebars');

  var noop = function(){};

  var PyrealView = Backbone.View.extend({
    name: "pyreal-view",
    template: null,
    templateId: null,
    elId: null,

    initialize: function() {
      if (this.elId == null)
        this.elId = '#'+this.name+'-container';
      this.setElement($(this.elId), false);
      if (this.templateId == null)
        this.templateId = '#'+this.name+'-template';

      this.postInitialize();
    },

    postInitialize: noop,

    getTemplateData: function() {
      var data;
      if (this.model) {
        data = this.model.toJSON();
      } else if (this.collection) {
        data = {collection: this.collection.toJSON()};
      }
      return data;
    },

    getTemplate: function() {
      return Handlebars.compile($(this.templateId).html());
    },

    getCachedTemplate: function() {
      if (this.template != null)
        return this.template;
      else
        return this.getTemplate();
    },

    getInnerHtml: function() {
      var data, template;

      data = this.getTemplateData();
      template = this.getCachedTemplate();
      return template(data);
    },

    render: function() {
      this.$el.html(this.getInnerHtml());
      this.$el.attr('data-view', this.name);
      this.postRender();
      return this;
    },

    postRender: noop

  });

  // might be useful some day
  var views = {};
  PyrealView.get = function (name) {
    return views[name];
  };
  PyrealView.create = function(name, opts) {
    opts = opts || {};
    return new views[name](opts);
  };
  PyrealView.register = function(props) {
    var view = PyrealView.extend(props);
    views[props.name] = view;
    return view;
  };

  // Handlebars helpers

  Handlebars.registerHelper('format-track', function(track) {
    track = parseInt(track);
    return (track < 10) ? '0'+track : track;
  });

  Handlebars.registerHelper('lib-link', function(path, name) {
    return new Handlebars.SafeString('<a href="#'+path+'">'+name+'</a>');
  });

  this.Pyreal = this.Pyreal || {};
  this.Pyreal.View = PyrealView;
}).call(this);
