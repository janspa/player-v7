App = Ember.Application.create();

App.Router.map(function() {
  this.resource('lib', function() {
    this.resource('artist', {path: ':artist'}, function() {
      this.resource('album', {path: ':album'});
    });
  });
});

App.LibRoute = Ember.Route.extend({
  model: function() {
    return $.ajax('/api/library/', { type:'GET' })
    .done(function(data) {
      return data;
    });
  },
  renderTemplate: function() {
    this.render({ outlet: 'lib' });
  }
});

App.ArtistRoute = Ember.Route.extend({
  model: function(params) {
    return $.ajax('/api/library/'+params.artist, { type:'GET' })
    .done(function(data) {
      console.log(data);
      return data;
    });
  },
  renderTemplate: function() {
    this.render({ outlet: 'lib' });
  }
});

App.AlbumRoute = Ember.Route.extend({
  model: function(params) {
    var artist = this.modelFor('artist');
    console.log('=>',artist);
    return $.ajax('/api/library/'+artist.get('id')+'/'+params.album, { type:'GET' })
    .done(function(data) {
      console.log(data);
      return data;
    });
  },
  renderTemplate: function() {
    this.render({ outlet: 'lib' });
  }
});

Ember.Handlebars.helper('format-track', function(track) {
  track = parseInt(track);
  return (track < 10) ? '0'+track : ''+track;
});