module.exports = function sanitize(input) {
  var sanitized = {};
  for (prop in input) {
    if (input.hasOwnProperty(prop)) {
      if (/^\$/.test(prop) == false)
        sanitized[prop] = input[prop];
    }
  }
  return sanitized;
};