var mongo = require('mongo-sync');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/pyreal');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Mongoose connected.");
});

var models = require('./models')(mongoose);

module.exports = mongoose;