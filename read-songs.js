var fs = require('fs');
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
var pathMod = require('path');
var progress = require('progress');
var argv = require('minimist')(process.argv.slice(2),
  { 'string': ['f', 'd'], 'boolean': ['r', 'dry-run'],
    'alias': {'f':'file', 'd':'dir', 'r':'recursive'} });
var tagReader = require('./tag-reader');
var mongoose = require('./db');
var dbFunctions = require('./db-functions');

var filetypeFilter = /(mp3|ogg)$/i;
var coverFilter = /^(cover|folder)\.(jpe?g|png)$/i;

var songsTotal = 0;

var dryRun = argv['dry-run'];

function readDir(path) {
  var contents = fs.readdirSync(path);
  var ret = {};
  for (var i = 0; i < contents.length; i++) {
    var item = contents[i];
    var stats = fs.statSync(pathMod.join(path, item));
    if (stats.isDirectory()) {
      if (!ret.dirs)
        ret.dirs = {};
      ret.dirs[item] = {};
    } else if (stats.isFile()) {
      if (filetypeFilter.test(item)) {
        if (!ret.songs)
          ret.songs = [];
        ret.songs.push(item);
      } else if (coverFilter.test(item)) {
        ret.cover = item;
      }
    }
  }
  if (ret.songs)
    songsTotal += ret.songs.length;
  return ret;
}

function walkDir (path) {
  var content = readDir(path);
  for (dir in content.dirs) {
    if (content.dirs.hasOwnProperty(dir)) {
      content.dirs[dir] = walkDir(pathMod.join(path, dir));
    }
  }
  return content;
}

function readTagsToDB (content, path, songDone, dirDone) {
  dirDone = dirDone || function(){};
  if (content.dirs) {
    var d = 0;
    var keys = Object.keys(content.dirs);
    var nextDir = function () {
      if (d < keys.length) {
        readTagsToDB(content.dirs[keys[d]], pathMod.join(path, keys[d]), songDone, function() {
          d++;
          nextDir();
        });
      }
    }
    nextDir();
  }
  if (content.songs) {
    var i = 0;
    var songData = null;
    var nextSong = function (err, product) {
      if (i < content.songs.length) {
        songData = tagReader.sync(pathMod.join(path, content.songs[i]));
        if (!dryRun)
          dbFunctions.addSong(mongoose, songData.normalized, nextSong);
        i++;
        songDone();
      } else if (content.cover && songData && !dryRun) {
        var coverData = {
          file: pathMod.join(path,content.cover),
          artist: songData.normalized.band || songData.normalized.artist,
          album: songData.normalized.album
         };
        dbFunctions.addCover(mongoose, coverData, dirDone);
      } else {
        dirDone();
      }
    };
    nextSong();
  }
}

function run() {
  if (argv.d) {
    console.log("Finding songs...");
    var path = argv.d;
    var dirContent = (argv.r) ? walkDir(path) : readDir(path);
    var q = "Found "+songsTotal+" songs. Do you want to process their tags and add to the database? y/n ";
    rl.question(q, function(answer) {
      var bar = new progress('reading song :current/:total [:bar] :percent', { total: songsTotal, incomplete: ' ', width: 20 });
      if (/^ye?s?/i.test(answer)) {
        readTagsToDB(dirContent, path, function() {
          bar.tick();
          if (bar.complete) {
            console.log("All done!");
          }
        });
      } else {
        console.log("Never mind then!");
      }
      rl.close();
    });
  }
  else if (argv.f) {
    tagReader(argv.f, function(err, data) {
      if (err)
        console.error(err);
      else
        console.log(data.normalized);
      rl.close();
    });
  }
  else {
    console.log("Usage:\n-f | --file <file path>\t\tScan a single file\n-d | --dir <directory path>\tScan a whole directory\n-r | --recursive\t\tScan subdirectories too (only with -d)");
    rl.close();
  }
}

run();