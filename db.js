var mongo = require('mongodb');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/pyreal');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var models = require('./models')(mongoose);

module.exports = mongoose;